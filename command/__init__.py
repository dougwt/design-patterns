from commands import *
from hardware import *


class RemoteControl():
    def __init__(self, size=7):
        self.size = size
        self.onCommands = [NoCommand() for i in range(self.size)]
        self.offCommands = [NoCommand() for i in range(self.size)]

    def setCommand(self, slot, onCommand, offCommand):
        self.onCommands[slot] = onCommand
        self.offCommands[slot] = offCommand

    def onButtonWasPushed(self, slot):
        self.onCommands[slot].execute()

    def offButtonWasPushed(self, slot):
        self.offCommands[slot].execute()

    # def __str__(self):
    #     output = ("-" * 10) + " Remote Control " + ("-" * 10)
    #     # for slot in range(self.size):
    #     #     output += "[slot " + slot + "]", self.onCommands[slot].getClass().getName() + "    " + self.offCommands[slot].getClass().getName()
    #     return output


# class RemoteControlWithUndo(RemoteControl):
#     def __init__(self, size=7):
#         RemoteControl.__init__(self, size)
#         self.undoCommand = NoCommand()

#     def onButtonWasPushed(self, slot):
#         self.onCommands[slot].execute()
#         self.undoCommand = self.onCommands[slot]

#     def offButtonWasPushed(self, slot):
#         self.offCommands[slot].execute()
#         self.undoCommand = self.offCommands[slot]

#     def undoButtonWasPushed(self):
#         self.undoCommand.undo()
#         self.undoCommand = NoCommand()


class RemoteControlWithUndoStack(RemoteControl):
    def __init__(self, size=7):
        RemoteControl.__init__(self, size)
        self.undoStack = []
        self.redoStack = []

    def onButtonWasPushed(self, slot):
        self.onCommands[slot].execute()
        self.undoStack.append(self.onCommands[slot])
        self.redoStack = []

    def offButtonWasPushed(self, slot):
        self.offCommands[slot].execute()
        self.undoStack.append(self.offCommands[slot])
        self.redoStack = []

    def undoButtonWasPushed(self):
        if len(self.undoStack) > 0:
            command = self.undoStack.pop()
            command.undo()
            self.redoStack.append(command)

    def redoButtonWasPushed(self):
        if len(self.redoStack) > 0:
            command = self.redoStack.pop()
            command.execute()
            self.undoStack.append(command)


# def RemoteLoader():
#     remoteControl = RemoteControl()

#     livingRoomLight = Light("Living Room Light")
#     kitchenLight = Light("Kitchen Light")
#     ceilingFan = CeilingFan("Living Room Fan")
#     garageDoor = GarageDoor("Garage Door")
#     stereo = Stereo("Living Room Stereo")

#     livingRoomLightOn = LightOnCommand(livingRoomLight)
#     livingRoomLightOff = LightOffCommand(livingRoomLight)
#     kitchenLightOn = LightOnCommand(kitchenLight)
#     kitchenLightOff = LightOffCommand(kitchenLight)

#     ceilingFanOn = CeilingFanOnCommand(ceilingFan)
#     ceilingFanOff = CeilingFanOffCommand(ceilingFan)

#     garageDoorUp = GarageDoorUpCommand(garageDoor)
#     garageDoorDown = GarageDoorDownCommand(garageDoor)

#     stereoOnWithCD = StereoOnWithCDCommand(stereo)
#     stereoOff = StereoOffCommand(stereo)

#     remoteControl.setCommand(0, livingRoomLightOn, livingRoomLightOff)
#     remoteControl.setCommand(1, kitchenLightOn, kitchenLightOff)
#     remoteControl.setCommand(2, ceilingFanOn, ceilingFanOff)
#     remoteControl.setCommand(3, garageDoorUp, garageDoorDown)
#     remoteControl.setCommand(4, stereoOnWithCD, stereoOff)

#     print remoteControl

#     remoteControl.onButtonWasPushed(0)
#     remoteControl.offButtonWasPushed(0)
#     remoteControl.onButtonWasPushed(1)
#     remoteControl.offButtonWasPushed(1)
#     remoteControl.onButtonWasPushed(2)
#     remoteControl.offButtonWasPushed(2)
#     remoteControl.onButtonWasPushed(3)
#     remoteControl.offButtonWasPushed(3)
#     remoteControl.onButtonWasPushed(4)
#     remoteControl.offButtonWasPushed(4)


def RemoteLoaderUndo():
    separator = "-"*20
    # We have a single remote control
    remoteControl = RemoteControlWithUndoStack()

    # Living Room light and On/Off Commands to control it
    livingRoomLight = Light("Living Room Light")
    livingRoomLightOn = LightOnCommand(livingRoomLight)
    livingRoomLightOff = LightOffCommand(livingRoomLight)

    # Kitchen light and on/off commands to control it
    kitchenLight = Light("Kitchen Light")
    kitchenLightOn = LightOnCommand(kitchenLight)
    kitchenLightOff = LightOffCommand(kitchenLight)

    # Bedroom light and on/off commands to control it
    bedroomLight = Light("Bedroom Light")
    bedroomLightOn = LightOnCommand(bedroomLight)
    bedroomLightOff = LightOffCommand(bedroomLight)

    ceilingFan = CeilingFan("Living Room Fan")
    ceilingFanOn = CeilingFanOnCommand(ceilingFan)
    ceilingFanOff = CeilingFanOffCommand(ceilingFan)

    stereo = Stereo("Living Room Stereo")
    stereoOnWithCD = StereoOnWithCDCommand(stereo)
    stereoOff = StereoOffCommand(stereo)

    partyOnCommand = MacroCommand([livingRoomLightOn, stereoOnWithCD, ceilingFanOn, kitchenLightOn])
    partyOffCommand = MacroCommand([kitchenLightOff, ceilingFanOff, stereoOff, livingRoomLightOff])

    # Map the on/off commands to "physical" buttons on the remote
    remoteControl.setCommand(0, livingRoomLightOn, livingRoomLightOff)
    remoteControl.setCommand(1, kitchenLightOn, kitchenLightOff)
    remoteControl.setCommand(2, bedroomLightOn, bedroomLightOff)
    remoteControl.setCommand(3, partyOnCommand, partyOffCommand)

    # Push each button once!
    remoteControl.onButtonWasPushed(0)   # LR On
    remoteControl.offButtonWasPushed(0)  # LR Off
    remoteControl.onButtonWasPushed(1)   # K On
    remoteControl.offButtonWasPushed(1)  # K Off
    remoteControl.onButtonWasPushed(2)   # BR On
    remoteControl.offButtonWasPushed(2)  # BR Off
    remoteControl.redoButtonWasPushed()  # Nothing
    print separator

    # Push Undo button repeatedly, stepping through the executed commands
    # in reverse chronological order
    remoteControl.undoButtonWasPushed()  # Undo BR Off
    remoteControl.undoButtonWasPushed()  # Undo BR On
    remoteControl.undoButtonWasPushed()  # Undo K Off
    remoteControl.undoButtonWasPushed()  # Undo K On
    remoteControl.undoButtonWasPushed()  # Undo LR Off
    remoteControl.undoButtonWasPushed()  # Undo LR On
    remoteControl.undoButtonWasPushed()  # Nothing
    remoteControl.undoButtonWasPushed()  # Nothing
    remoteControl.undoButtonWasPushed()  # Nothing
    remoteControl.undoButtonWasPushed()  # Nothing
    print separator

    # Push each button again
    remoteControl.onButtonWasPushed(0)   # LR On
    remoteControl.offButtonWasPushed(0)  # LR Off
    remoteControl.onButtonWasPushed(1)   # K On
    remoteControl.offButtonWasPushed(1)  # K Off
    remoteControl.onButtonWasPushed(2)   # BR On
    remoteControl.offButtonWasPushed(2)  # BR Off

    # Push each button again
    remoteControl.onButtonWasPushed(0)   # LR On
    remoteControl.offButtonWasPushed(0)  # LR Off
    remoteControl.onButtonWasPushed(1)   # K On
    remoteControl.offButtonWasPushed(1)  # K Off
    remoteControl.onButtonWasPushed(2)   # BR On
    remoteControl.offButtonWasPushed(2)  # BR Off
    print separator

    # Undo the last 4 commands
    remoteControl.undoButtonWasPushed()  # Undo BR Off
    remoteControl.undoButtonWasPushed()  # Undo BR On
    remoteControl.undoButtonWasPushed()  # Undo K Off
    remoteControl.undoButtonWasPushed()  # Undo K On
    print separator

    # Redo the last 4 commands
    remoteControl.redoButtonWasPushed()  # Redo K On
    remoteControl.redoButtonWasPushed()  # Redo K Off
    remoteControl.redoButtonWasPushed()  # Redo BR On
    remoteControl.redoButtonWasPushed()  # Redo BR Off
    remoteControl.redoButtonWasPushed()  # Nothing
    print separator

    remoteControl.onButtonWasPushed(3)
    print separator

    remoteControl.offButtonWasPushed(3)
    print separator

    remoteControl.undoButtonWasPushed()
    print separator

    remoteControl.redoButtonWasPushed()

RemoteLoaderUndo()

