class Thing():
    def __init__(self, title):
        self.title = title
        self.isOn = False

    def on(self):
        self.isOn = True
        print self.title, "is on."

    def off(self):
        self.isOn = False
        print self.title, "is off."


class Light(Thing):
    pass


class CeilingFan(Thing):
    pass


class GarageDoor(Thing):
    def open(self):
        self.on()
        print self.title, "is open."

    def close(self):
        self.off()
        print self.title, "is closed."


class CeilingFan(Thing):
    def __init__(self, title):
        Thing.__init__(self, title)
        self.speed = 0

    def high(self):
        self.speed = 3
        print self.title, "is set to HIGH."

    def medium(self):
        self.speed = 2
        print self.title, "is set to MEDIUM."

    def low(self):
        self.speed = 1
        print self.title, "is set to LOW."

    def off(self):
        self.speed = 0
        print self.title, "is OFF."

    def getSpeed(self):
        return self.speed


class Stereo(Thing):
    def __init__(self, title):
        Thing.__init__(self, title)
        self.isOn = False
        self.volume = 0
        self.disc = None

    def setCD(self, disc):
        self.disc = disc

    def ejectCD(self):
        self.disc = None

    def setVolume(self, volume):
        self.volume = volume
