class Command():
    def execute(self):
        pass

    def undo(self):
        pass


class NoCommand(Command):
    pass


class LightOnCommand(Command):
    def __init__(self, light):
        self.light = light

    def execute(self):
        self.light.on()

    def undo(self):
        self.light.off()


class LightOffCommand(Command):
    def __init__(self, light):
        self.light = light

    def execute(self):
        self.light.off()

    def undo(self):
        self.light.on()


class CeilingFanOnCommand(Command):
    def __init__(self, fan):
        self.fan = fan

    def execute(self):
        self.fan.on()

    def undo(self):
        self.fan.off()


class CeilingFanOffCommand(Command):
    def __init__(self, fan):
        self.fan = fan

    def execute(self):
        self.fan.off()

    def undo(self):
        self.fan.on()


class GarageDoorUpCommand(Command):
    def __init__(self, door):
        self.door = door

    def execute(self):
        self.door.open()

    def undo(self):
        self.door.close()


class GarageDoorDownCommand(Command):
    def __init__(self, door):
        self.door = door

    def execute(self):
        self.door.close()

    def undo(self):
        self.door.open()


class StereoOnWithCDCommand(Command):
    def __init__(self, stereo):
        self.stereo = stereo

    def execute(self):
        self.stereo.on()
        self.stereo.setCD(1)
        self.stereo.setVolume(11)

    def undo(self):
        self.stereo.off()
        self.stereo.ejectCD()


class StereoOffCommand(Command):
    def __init__(self, stereo):
        self.stereo = stereo

    def execute(self):
        self.stereo.off()

    def undo(self):
        self.stereo.on()

class MacroCommand(Command):
    def __init__(self, commands):
        self.commands = []
        for command in commands:
            if isinstance(command, Command):
                self.commands.append(command)

    def execute(self):
        for command in self.commands:
            if isinstance(command, Command):
                command.execute()

    def undo(self):
        for command in reversed(self.commands):
            if isinstance(command, Command):
                command.undo()
