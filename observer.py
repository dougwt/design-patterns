class Subject():
    """Abstract base class for the subject being observed."""
    def registerObserver(self, observer):
        """Adds an observer to the collection."""
        pass

    def removeObserver(self, observer):
        """Removes an observer from the collection."""
        pass

    def notifyObservers(self):
        """Notifies all observers in collection."""
        pass


class Observer():
    """Abstract base class for objects doing the observing."""
    def update(self, subject):
        """Called by notifyObservers whenever the Subject's state changes."""
        pass


class DisplayElement():
    """Abstract base class for a Display."""
    def display(self):
        """Updates the info being displayed."""
        pass


class WeatherData(Subject):
    """Concrete class representing the Subject being observed.

    Simulates a device used to track/record the current temperature, humidity, and pressure.
    """
    def __init__(self, temperature=0, humidity=0, pressure=0):
        """Initializes the device with zero-value attributes."""
        self.observers = []
        self.setMeasurements(temperature, humidity, pressure)

    def registerObserver(self, observer):
        """Adds observer object to the collection of observers."""
        if isinstance(observer, Observer):
            self.observers.append(observer)

    def removeObserver(self, observer):
        """Removes observer object from the collection of observers."""
        if self.observers.__contains__(observer):
            self.observers.remove(observer)

    def notifyObservers(self):
        """Notifies all observers of the state change."""
        if self.changed:
            for observer in self.observers:
                observer.update(self)
            self.changed = False;

    def getTemperature(self):
        return self.temperature

    def getHumidity(self):
        return self.humidity

    def getPressure(self):
        return self.pressure

    def measurementsChanges(self):
        """To be called anytime the state changes. Kicks off notifyObservers."""
        self.notifyObservers()

    def setMeasurements(self, temperature, humidity, pressure):
        """Used to simulate changing measurements in a real-world scenario."""
        self.temperature = temperature
        self.humidity = humidity
        self.pressure = pressure
        self.setChanged()
        self.measurementsChanges()
        print

    def setChanged(self):
        """Sets changed flag to true, so notifyObservers can be performed."""
        self.changed = True;


class CurrentConditionsDisplay(Observer, DisplayElement):
    """Simulates a display for monitoring the current conditions."""
    def __init__(self, weatherData):
        self.weatherData = weatherData
        self.temperature = 0
        self.humidity = 0
        weatherData.registerObserver(self)

    def update(self, subject):
        if isinstance(subject, WeatherData):
            self.temperature = subject.getTemperature()
            self.humidity = subject.getHumidity()
            self.display()

    def display(self):
        output = u"Current conditions: %d\u00b0F and %d%% humidity"
        print output % (self.temperature, self.humidity)


class StatisticsDisplay(Observer, DisplayElement):
    """Simulates a display for monitoring weather statistics."""
    def __init__(self, weatherData):
        self.weatherData = weatherData
        self.maxTemp = 0
        self.minTemp = 9999
        self.tempSum = 0
        self.numReadings = 0
        weatherData.registerObserver(self)

    def update(self, subject):
        if isinstance(subject, WeatherData):
            self.weatherData = subject
            temp = weatherData.getTemperature()
            self.tempSum += temp
            self.numReadings += 1

            if temp > self.maxTemp:
                self.maxTemp = temp

            if temp < self.minTemp:
                self.minTemp = temp

            self.display()

    def display(self):
        print "Avg/Max/Min temperature = %d/%d/%d" % ((self.tempSum / self.numReadings), self.maxTemp, self.minTemp)


class ForcastDisplay(Observer, DisplayElement):
    """Simulates a display for monitoring weather forcast."""
    def __init__(self, weatherData):
        self.weatherData = weatherData
        self.currentPressure = 0
        self.lastPressure = 0
        weatherData.registerObserver(self)

    def update(self, subject):
        if isinstance(subject, WeatherData):
            self.weatherData = subject
            self.lastPressure = self.currentPressure
            self.currentPressure = self.weatherData.getPressure()
            self.display()

    def display(self):
        print "Forecast:",
        if self.currentPressure > self.lastPressure:
            print "Improving weather on the way!"
        elif self.currentPressure == self.lastPressure:
            print "More of the same!"
        elif self.currentPressure < self.lastPressure:
            print "Watch out for cooler, rainy weather!"


class HeatIndexDisplay(Observer, DisplayElement):
    """Simulates a display for monitoring the heat index."""
    def __init__(self, weatherData):
        self.weatherData = weatherData
        self.heatIndex = 0
        weatherData.registerObserver(self)

    def update(self, subject):
        if isinstance(subject, WeatherData):
            self.weatherData = subject
            t = weatherData.getTemperature()
            rh = weatherData.getHumidity()
            self.heatIndex = (
                (16.923 + (0.185212 * t)) +
                (5.37941 * rh) -
                (0.100254 * t * rh) +
                (0.00941695 * (t * t)) +
                (0.00728898 * (rh * rh)) +
                (0.000345372 * (t * t * rh)) -
                (0.000814971 * (t * rh * rh)) +
                (0.0000102102 * (t * t * rh * rh)) -
                (0.000038646 * (t * t * t)) +
                (0.0000291583 * (rh * rh * rh)) +
                (0.00000142721 * (t * t * t * rh)) +
                (0.000000197483 * (t * rh * rh * rh)) -
                (0.0000000218429 * (t * t * t * rh * rh)) +
                (0.000000000843296 * (t * t * rh * rh * rh)) -
                (0.0000000000481975 * (t * t * t * rh * rh * rh)))
            self.display()

    def display(self):
        print u"Head index is %d\u00b0F" % self.heatIndex

# instantiate WeatherData device simulating physical hardware sensors
weatherData = WeatherData()

# instantiate various displays (aka observers). Each will be notified anytime the weather conditions change.
currentDisplay = CurrentConditionsDisplay(weatherData)
statisticsDisplay = StatisticsDisplay(weatherData)
forecastDisplay = ForcastDisplay(weatherData)
heatIndexDisplay = HeatIndexDisplay(weatherData)

# simulate changing weather conditions
weatherData.setMeasurements(80, 65, 30.4)
weatherData.setMeasurements(82, 70, 29.2)
weatherData.setMeasurements(78, 90, 29.2)
